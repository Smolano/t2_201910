package model.data_structures;

import org.junit.Test;

import static org.junit.Assert.*;



public class LinkedListTest {

  private LinkedList<String> list;

  public static final String[] PREARRAY = {"0x", "1x", "2x", "3x"};

  private void setup1(){

    list = new LinkedList<>();
    for(int i = 0; i < 10; i++){
      list.add(i + " n");
    }
  }

  private void setup2(){

    list = new LinkedList<>();
    for(int i = 0; i < 4; i++){
      list.add(i + "x");
    }
  }

  @Test
  public void getElement() {

    setup2();
    for(int i = 0; i < PREARRAY.length; i++) {
      assertEquals("Failed to get element at list index " + i, list.getElement(i), PREARRAY[i]);
    }

  }

  @Test
  public void getSize() {
    list = new LinkedList<>();
    assert list.getSize() == 0;

    setup1();
    assert list.getSize() == 10;

  }

  @Test
  public void add() {

    setup1();
    assert "0 n".equals(list.get(0));

    assert "9 n".equals(list.get(9));

    setup2();
    for(int i = 0; i < PREARRAY.length; i++){
      assert list.get(i).equals(PREARRAY[i]);
    }
  }

  @Test
  public void addAtEnd() {

    setup1();
    list.addAtEnd("last");
    assert list.get(list.getSize() - 1).equals("last");

  }

  @Test
  public void addAtK() {

    setup1();
    list.addAtK("new", 5);
    assert list.get(5).equals("new");

    try{
      list.addAtK("Out of bounds", 15);
      fail("Should throw exception");
    }
    catch(Exception e){

    }


  }

  @Test
  public void delete() {

    setup1();
    list.delete("1 n");
    assertTrue(!list.contains("1 n"));

  }

  @Test
  public void deleteAtK() {
    setup1();
    list.deleteAtK(0);
    list.deleteAtK(0);
    assertEquals("List size should have decreased", 8, (int) list.getSize());
    assertEquals("Failed to delete first element", list.get(0), "2 n");

    list.deleteAtK(list.getSize() - 1);
    assertEquals("Failed to delete last element", list.get(list.getSize() - 1), "8 n");
  }

  @Test
  public void next() {

    setup1();
    assertEquals("Failed to get the next value", list.next(), list.get(1));
    assertEquals("Failed to get the next value", list.next(), list.get(2));
    assertEquals("Failed to get the next value", list.next(), list.get(3));

    list = new LinkedList<>();
    list.add("Hello");
    assertNull(list.next());

  }

  @Test
  public void previous() {

    setup1();
    list.next();
    assertEquals("Failed to get the previous value", list.previous(), list.get(0));
  }

  @Test
  public void getCurrentElement() {

    setup1();
    assertEquals(list.getCurrentElement(), "0 n");
    list.next();
    assertEquals(list.getCurrentElement(), "1 n");
    list.next();
    list.next();
    assertEquals(list.getCurrentElement(), "3 n");
    list.previous();
    assertEquals(list.getCurrentElement(), "2 n");


  }


}