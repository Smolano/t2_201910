package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	Integer getSize();

	boolean add(T newObject);

	void addAtEnd(T newObject);

	void addAtK(T neObject, int index);

	T getElement(int index);

	T getCurrentElement();

	void delete(T oldObject);

	void deleteAtK(int index);

	T next();

	T previous();

}
