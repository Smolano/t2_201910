package model.logic;

import api.IMovingViolationsManager;
import com.opencsv.CSVReader;
import java.io.FileReader;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {

  private CSVReader reader;

  private LinkedList<VOMovingViolations> violationsList;

  public MovingViolationsManager() {

    violationsList = new LinkedList<>();

  }

  /**
   * Returns a boolean value indicating whether the violation was accident or not
   * @param indicator Yes or No value
   * @return true if it was an accident, false if not
   * @throws Exception if the value is not either 'yes' or 'no' or any capped variation of the two
   */
  private boolean indicatesAccident(String indicator) throws Exception {

    if (indicator.toLowerCase().equals("yes")) {
      return true;
    } else if (indicator.toLowerCase().equals("no")) {
      return false;
    } else {
      throw new Exception("Invalid ACCIDENTINDICATOR value");
    }

  }

  public void loadMovingViolations(String movingViolationsFile) {
    // TODO Auto-generated method stub
    try {
      reader = new CSVReader(new FileReader(movingViolationsFile));
      String[] firstLine = reader.readNext();
      String[] line = firstLine;
      //Indicates the number of items to load all set to: while(line != null){} to get all data
      for (int i = 0; i < 10; i++) {
        try {
          line = reader.readNext();
          VOMovingViolations movingViolation = new VOMovingViolations(Integer.valueOf(line[0]),
              line[2],
              Integer.valueOf(line[3]),
              Integer.valueOf(line[4]),
              Float.valueOf(line[5]),
              Float.valueOf(line[6]),
              line[7],
              Integer.valueOf(line[8]),
              Integer.valueOf(line[9]),
              Integer.valueOf(line[10]),
              line[12],
              line[13],
              line[14],
              line[15]);
          violationsList.add(movingViolation);
        }
        //Skips any unidentified data
        catch (Exception e) {

        }


      }

      System.out.println(violationsList.get(0));
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Override
  public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode(String violationCode) {
    // TODO Auto-generated method stub
    LinkedList<VOMovingViolations> newList = new LinkedList<>();
    for(VOMovingViolations violation: violationsList){
      if(violation.getViolationCode().equals(violationCode)){
        newList.add(violation);
      }
    }
    return newList;
  }

  @Override
  public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
    // TODO Auto-generated method stub
      LinkedList<VOMovingViolations> newList = new LinkedList<>();
      for (VOMovingViolations violation : violationsList) {
        if (violation.getAccidentIndicator().equals(accidentIndicator)) {
          newList.add(violation);
        }
      }
      return newList;
  }

  public static void main(String[] args) {

    MovingViolationsManager movingViolations = new MovingViolationsManager();
    movingViolations.loadMovingViolations("./data/Moving_violations.csv");

  }


}
