package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId;

	private String location;

	private int addressId;

	private int streetSegId;

	private float xCoord;

	private float yCoord;

	private String type;

	private int fineAMT;

	private int totalPaid;

	private int penalty1;

	private String accidentIndicator;

	private String ticketIssue;

	private String violationCode;

	private String violationDesc;

	public VOMovingViolations(int objectId, String location, int addressId, int streetSegId,
			float xCoord, float yCoord, String type, int fineAMT, int totalPaid, int penalty1,
			String accidentIndicator, String ticketIssue, String violationCode,
			String violationDesc) {
		this.objectId = objectId;
		this.location = location;
		this.addressId = addressId;
		this.streetSegId = streetSegId;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.type = type;
		this.fineAMT = fineAMT;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssue = ticketIssue;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssue;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}

	public int getObjectId() {
		return objectId;
	}

	public int getAddressId() {
		return addressId;
	}

	public int getStreetSegId() {
		return streetSegId;
	}

	public float getxCoord() {
		return xCoord;
	}

	public float getyCoord() {
		return yCoord;
	}

	public String getType() {
		return type;
	}

	public int getFineAMT() {
		return fineAMT;
	}

	public int getPenalty1() {
		return penalty1;
	}

	public String getTicketIssue() {
		return ticketIssue;
	}

	public String getViolationCode() {
		return violationCode;
	}


	@Override
	public String toString() {
		String ans = "";
		ans += objectId + " " + location + " " + accidentIndicator;
		return ans;
	}
}
